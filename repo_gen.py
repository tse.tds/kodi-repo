# -*- coding: utf-8 -*-

import xml.etree.ElementTree
import os
import sys
import zipfile
import ftplib
import time
import shutil
import hashlib


cwd = os.path.dirname(os.path.abspath(__file__))
builddir = os.path.join(cwd, 'build')
zipdir = os.path.join(builddir, 'zips')

if not os.path.exists(zipdir):
    os.makedirs(zipdir)

addons_xml = u"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<addons>\n"

for x in os.listdir(cwd):
    full_path = os.path.join(cwd, x)
    if os.path.exists(os.path.join(full_path, 'addon.xml')):
        print 'Processing: %s' % x
        xml_lines = open(os.path.join(full_path, 'addon.xml'), "r").read().splitlines()
        addon_xml = ''
        for line in xml_lines:
            if line.find( "<?xml" ) >= 0: continue
            addon_xml += unicode(line.rstrip() + "\n", "UTF-8")
        addons_xml += addon_xml.rstrip() + "\n\n"

        for dirname, subdirs, files in os.walk(full_path):
            for fl in files:
                if fl.lower().endswith('.pyo'):
                    os.remove(os.path.join(dirname, fl))
        e = xml.etree.ElementTree.parse(os.path.join(full_path, 'addon.xml')).getroot()
        if not os.path.exists(os.path.join(zipdir, x)):
            os.makedirs(os.path.join(zipdir, x))
        zip_file = os.path.join(os.path.join(zipdir, x), '%s-%s.zip' % (x, e.get('version')))
        icon_file_src = os.path.join(full_path, 'icon.png')
        icon_file_dst = os.path.join(os.path.join(zipdir, x), 'icon.png')
        if os.path.exists(icon_file_src):
            shutil.copy2(icon_file_src, icon_file_dst)
        if not os.path.exists(zip_file):
            print 'Making %s' % zip_file
            zf = zipfile.ZipFile(zip_file, 'w', zipfile.ZIP_DEFLATED)
            for dirname, subdirs, files in os.walk(full_path):
                if '.idea' in dirname or '.svn' in dirname:
                    continue
                for filename in files:
                    absname = os.path.abspath(os.path.join(dirname, filename))
                    arcname = absname[len(full_path) - len(x):]
                    zf.write(absname, arcname)
            zf.close()

addons_xml = addons_xml.strip() + u"\n</addons>\n"
print 'Creating addons.xml'
open(os.path.join(builddir, 'addons.xml'), "w").write(addons_xml.encode('utf-8'))
print 'Creating addons.xml.md5'
open(os.path.join(builddir, 'addons.xml.md5'), "w").write(hashlib.md5(open(os.path.join(builddir, 'addons.xml'), 'rb').read()).hexdigest())



